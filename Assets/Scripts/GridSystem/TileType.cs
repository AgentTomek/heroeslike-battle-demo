﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TileType : MonoBehaviour
{
   public enum tileTerrainType
    {
        Grass = 0,
        Forest = 1,
        Mountains = 2,
        Rivers = 3
    }

}
