﻿using Assets.Scripts.TileSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TileType;

public class GridSystem : Singleton<GridSystem>
{
    [SerializeField] public int mapSizeX = 8;
    [SerializeField] public int mapSizeY = 8;
    [SerializeField] private List<GameObject> tilePrefabs = new List<GameObject>();
    [SerializeField] private List<tileTerrainType> mapTiles = new List<tileTerrainType>();

    public Tile[,] tiles;
    public void InitGrid()
    {
        GenerateMapData();
    }
    void GenerateMapData()
    {
        tiles = new Tile[mapSizeX, mapSizeY];

        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                var index = mapSizeX * x + y;
                var tile = tilePrefabs.Find(tilePrefab => tilePrefab.GetComponent<Tile>().terrainType == mapTiles[index]);
                var createdTile = Instantiate(tile, new Vector3(x, y, 0), Quaternion.identity);
                createdTile.transform.parent = gameObject.transform;
                tiles[x, y] = createdTile.GetComponent<Tile>();
            }
        }
    }
    public List<Tile> CheckForReachableTiles(Vector3 clickedTilePos, int avalibleUnitMovePoints)
    {
        List<Tile> reachableTiles = new List<Tile>();

        for (int _x = 0; _x < mapSizeX; _x++)
        {
            for (int _y = 0; _y < mapSizeY; _y++)
            {
                var tile = tiles[_x, _y];

                if (Vector3.Distance(tile.transform.position, clickedTilePos) <= avalibleUnitMovePoints + 1)
                {
                    reachableTiles.Add(tile);
                }
            }
        }
        return reachableTiles;
    }
    public void DisableReachableTileMarker()
    {
        for (int _x = 0; _x < mapSizeX; _x++)
        {
            for (int _y = 0; _y < mapSizeY; _y++)
            {
                var tile = tiles[_x, _y];
                if (tile.IsReachable == true)
                {
                    tile.SetReachable(false);
                }
            }
        }
    }
}
