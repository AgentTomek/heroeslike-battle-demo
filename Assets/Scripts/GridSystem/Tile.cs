﻿using System.Collections;
using UnityEngine;
using static TileType;

namespace Assets.Scripts.TileSystem
{
    public class Tile : MonoBehaviour, IGridTile
    {
        public tileTerrainType terrainType;

        [SerializeField] private bool isTileReachable = false;

        public bool IsReachable => isTileReachable;

        [SerializeField] GameObject selectedIcon;
        public void SetReachable(bool isReachable)
        {
            this.isTileReachable = isReachable;
            selectedIcon.SetActive(isReachable);
        }

        public Vector3 GetTilePos()
        {
            return this.transform.position;
        }
        public bool CheckIfReachable()
        {
            return isTileReachable;
        }
    }
}