﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class GridInteractionSystem : Singleton<GridInteractionSystem>
{
    [SerializeField] BattleManager tempBattleManager;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            List<RaycastResult> clickedStack = CheckForStackOfObjectsWithRaycast();

            if (clickedStack.Any(x => x.gameObject.GetComponent<IGridElement>() != null))
            {
                tempBattleManager.OnGridInteraction(clickedStack);
            }
        }
    }
    private List<RaycastResult> CheckForStackOfObjectsWithRaycast()
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };
        pointerData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);

        return results;
    }
}
