﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType
{
    Swordman,
    Archer,
    Cavalry,
    Pikeman

}
[CreateAssetMenu(fileName = "UnitData", menuName = "SpriptableObjects/UnitData")]

public class UnitData : ScriptableObject
{
    public string UnitName => unitName;
    public string Description => description;
    public int Health => health;
    public int AttackRange => attackRange;
    public int AttackValue => attackValue;
    public int MovePoints => movePoints;
    public int ActionPoints => actionPoints;

    [SerializeField] private string unitName;
    [SerializeField] private string description;
    [SerializeField] private int health;
    [SerializeField] private int attackRange;
    [SerializeField] private int attackValue;
    [SerializeField] private int movePoints;
    [SerializeField] private int actionPoints;
}
