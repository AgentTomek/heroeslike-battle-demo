﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectingUnit : State
{
    public SelectingUnit(BattleManager battleManager) : base(battleManager) { }

    public override IEnumerator OnStart()
    {
        battleManager.UpdateInfoText("Wybierz jednostkę");
        return base.OnStart();
    }
    public override IEnumerator OnGridInteraction(List<RaycastResult> clickedStack)
    {
        RaycastResult foundUnit = clickedStack.Find(x => x.gameObject.GetComponent<IGridUnit>() != null);
        if (foundUnit.gameObject != null && foundUnit.gameObject.GetComponent<IGridUnit>().GetUnitSide() == battleManager.GetCurrentPlayer())
        {
            battleManager.selectedUnit = foundUnit.gameObject.GetComponent<IGridUnit>();
            battleManager.SetState(new UnitAction(battleManager));
        }
        return base.OnGridInteraction(clickedStack);
    }
}
