﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : State
{
    public Win(BattleManager battleManager) : base(battleManager) { }

    public override IEnumerator OnStart()
    {
        battleManager.UpdateInfoText("Wygrał gracz: " + battleManager.GetCurrentPlayer());
        return base.OnStart();
    }
}
