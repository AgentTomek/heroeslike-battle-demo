﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleInit : State
{
    public BattleInit(BattleManager battleManager) : base(battleManager) { }

    public override IEnumerator OnStart()
    {
        GridSystem.GetInstance().InitGrid();
        battleManager.SetCurrentPlayer(BattleManager.UnitSide.Player1); // TODO zrobić jakąś funkcję która ustawia gracza losowo
        battleManager.ActivateUnits();
        battleManager.SetPlayerActionPoints();
        battleManager.UpdateActionPointsText();
        battleManager.UpdateTurnText();
        battleManager.SetState(new SelectingUnit(battleManager));
        return base.OnStart();
    }
}
