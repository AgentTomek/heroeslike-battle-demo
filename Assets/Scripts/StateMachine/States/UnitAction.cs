﻿using Assets.Scripts.TileSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitAction : State
{
    public UnitAction(BattleManager battleManager) : base(battleManager) { }

    public override IEnumerator OnStart()
    {
        battleManager.UpdateInfoText("Rusz się lub zaatakuj!");
        List<Tile> reachableTileForUnit = GridSystem.GetInstance().CheckForReachableTiles(battleManager.selectedUnit.GetCurrentPosition(), battleManager.selectedUnit.GetUnitMovePoints());
        reachableTileForUnit.ForEach(x => x.SetReachable(true));
        return base.OnStart();
    }
    public override IEnumerator OnGridInteraction(List<RaycastResult> clickedStack)
    {
        RaycastResult foundTile = clickedStack.Find(x => x.gameObject.GetComponent<IGridTile>() != null);
        if (foundTile.gameObject != null && battleManager.playerActionPoints > 0) 
        {
            RaycastResult _foundUnit = clickedStack.Find((x => x.gameObject.GetComponent<IGridUnit>() != null));
            if (_foundUnit.gameObject != null)
            {
                IGridUnit foundUnit = _foundUnit.gameObject.GetComponent<IGridUnit>();
                if (foundUnit.GetUnitSide() == battleManager.GetCurrentPlayer())
                {
                    if (foundUnit != battleManager.selectedUnit)
                    {
                        GridSystem.GetInstance().DisableReachableTileMarker();
                        battleManager.selectedUnit = foundUnit;
                    }
                    battleManager.SetState(new UnitAction(battleManager));
                }
                else
                {
                    if (TargetingSystem.GetInstance().CheckIfUnitIsProperTarget(foundTile, _foundUnit) == true)
                    {
                        battleManager.UpdateInfoText("Przeciwnik trafiony!");
                        battleManager.DecreasePlayerActionPoints();
                        UnitActionSystem.GetInstance().UnitAttack(battleManager.selectedUnit, foundUnit);
                    }
                }
            }
            else
            {
                if (foundTile.gameObject.GetComponent<IGridTile>().CheckIfReachable() == true)
                {
                    battleManager.DecreasePlayerActionPoints();
                    UnitActionSystem.GetInstance().UnitMove(battleManager.selectedUnit, foundTile);
                    GridSystem.GetInstance().DisableReachableTileMarker();
                    battleManager.SetState(new UnitAction(battleManager));
                }
            }
        }   else
        {
            GridSystem.GetInstance().DisableReachableTileMarker();
            battleManager.UpdateInfoText("Nie masz już punktów akcji, kliknij NEXT TURN!");
        }
        return base.OnGridInteraction(clickedStack);
    }
}
