﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartNewTurn : State
{   public StartNewTurn(BattleManager battleManager) : base(battleManager) { }

    public override IEnumerator OnStart()
    {
        battleManager.SetState(new SelectingUnit(battleManager));
        GridSystem.GetInstance().DisableReachableTileMarker();
        battleManager.SetPlayerActionPoints();
        battleManager.UpdateActionPointsText();
        battleManager.ChangeActivePlayer();
        battleManager.UpdateTurnText();
        return base.OnStart();
    }
}
