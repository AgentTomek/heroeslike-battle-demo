﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class State
{
    protected BattleManager battleManager;
    
    public State (BattleManager battleManager)
    {
        this.battleManager = battleManager;
    }
    public virtual IEnumerator OnStart()
    {
        yield break;
    }
    public virtual IEnumerator OnGridInteraction(List<RaycastResult> clickedStack)
    {
        yield break;
    }
}
