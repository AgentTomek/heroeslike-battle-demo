﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour // tymczasowy system
{
    [SerializeField] BattleManager battleManager;

    private List<IGridUnit> playerOneUnits = new List<IGridUnit>();
    private List<IGridUnit> playerTwoUnits = new List<IGridUnit>();

    void Start()
    {
        UnitCounter();
    }

    private void UnitCounter()
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject.GetComponent<IGridUnit>() != null)
            {
                if (child.gameObject.GetComponent<IGridUnit>().GetUnitSide() == BattleManager.UnitSide.Player1)
                {
                    playerOneUnits.Add(child.gameObject.GetComponent<IGridUnit>());
                }
                else
                {
                    playerTwoUnits.Add(child.gameObject.GetComponent<IGridUnit>());
                }
            }
            else
            {
                break;
            }
        }
    }
    public void UpdateUnitCount(IGridUnit unitWhichDied)
    {
        if(unitWhichDied != null)
        {
            if (unitWhichDied.GetUnitSide() == BattleManager.UnitSide.Player1)
            {
                playerOneUnits.Remove(unitWhichDied);
            }
            else
            {
                playerTwoUnits.Remove(unitWhichDied);
            }
        }
    }
    public void CheckIfCurrentTeamWins()
    {
        if (playerOneUnits.Count == 0 || playerTwoUnits.Count == 0)
        {
            battleManager.SetState(new Win(battleManager));
        }
        else
        {
            return;
        }
    }
}
