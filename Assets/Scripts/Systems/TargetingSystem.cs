﻿using Assets.Scripts.TileSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TargetingSystem : Singleton<TargetingSystem>
{
    [SerializeField] GridSystem tempGridSystem;
    [SerializeField] BattleManager tempBattleManager;
    void DrawUnitAttackRange(IGridUnit selectedUnit) //TODO zainplemetnować ją na początku tempUnitAction
    {

    }
    List<Tile> CheckUnitAttackRange(IGridUnit selectedUnit)
    {
        List<Tile> reachableTilesForAttack = new List<Tile>();

        for (int _x = 0; _x < tempGridSystem.mapSizeX; _x++)
        {
            for (int _y = 0; _y < tempGridSystem.mapSizeY; _y++)
            {
                var tile = tempGridSystem.tiles[_x, _y];

                if (Vector3.Distance(tile.transform.position, selectedUnit.GetCurrentPosition()) <= selectedUnit.GetUnitAttackRange() +1)
                {
                    reachableTilesForAttack.Add(tile);
                }
            }
        }
        return reachableTilesForAttack;
    }
    public bool CheckIfUnitIsProperTarget(RaycastResult foundTile, RaycastResult target)
    {
        Tile _foundTile = foundTile.gameObject.GetComponent<Tile>();
        List<Tile> reachableTilesForAttack = CheckUnitAttackRange(tempBattleManager.selectedUnit);
        if (reachableTilesForAttack.Contains(_foundTile))
        {
            return true;
        }
        else
        {
            Debug.Log("Target is beyond range");
            return false;
        }
    }
}
