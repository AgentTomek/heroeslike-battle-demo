﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitActionSystem : Singleton<UnitActionSystem>
{
    [SerializeField] private UnitManager unitManager;

    public void UnitMove (IGridUnit currentUnit, RaycastResult clickedTile)
    {
        Vector3 unitPos = currentUnit.GetUnitPos();
        Vector3 tilePos = clickedTile.gameObject.GetComponent<IGridTile>().GetTilePos();
        Vector3 tileToMovePos = new Vector3(tilePos.x, tilePos.y, unitPos.z);
        currentUnit.SetUnitPos(tileToMovePos);
    }

    public void UnitAttack(IGridUnit currentUnit, IGridUnit target)
    {
        if (target.GetUnitCurrentHealth() > currentUnit.GetUnitAttackValue())
        {
            target.SetUnitHealthAfterAttack(currentUnit.GetUnitAttackValue());
        }   
        else
        {
            if(target != null)
            {
                unitManager.UpdateUnitCount(target);
                target.DestroyUnitWhenDied();
                unitManager.CheckIfCurrentTeamWins();
            }
        }
    }
}
