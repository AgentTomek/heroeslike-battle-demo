﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class BattleManager : StateMachine // TODO wyodrębnić system dla inputów, żeby zrobić porządek w tej klasie
{
    public enum UnitSide
    {
        Player1,
        Player2
    }
    public int playerActionPoints;

    [SerializeField] TMP_Text turnText;
    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_Text actionPointsText;


    public IGridUnit selectedUnit;
    private UnitSide currentPlayer;
    GameObject units;
    public void OnGridInteraction(List<RaycastResult> clickedStack)
    {
        StartCoroutine(state.OnGridInteraction(clickedStack));
    }
    public void ChangeActivePlayer()
    {
        switch (currentPlayer)
        {
            case UnitSide.Player1:
                currentPlayer = UnitSide.Player2;
                break;
            case UnitSide.Player2:
                currentPlayer = UnitSide.Player1;
                break;
        }
    }
    public void ChangeTurnButton()
    {
        SetState(new StartNewTurn(this));
    }
    public void SetCurrentPlayer(UnitSide unitSide)
    {
        currentPlayer = unitSide;
    }
    public UnitSide GetCurrentPlayer()
    {
        return currentPlayer;
    }
    public void StartGameButton()
    {
        SetState(new BattleInit(this));
    }
    public void ActivateUnits()
    {
        units = this.transform.Find("Units").gameObject;
        if (units.activeSelf == false)
        {
            units.SetActive(true);
        }
    }
    public void DecreasePlayerActionPoints()
    {
        playerActionPoints--;
        UpdateActionPointsText();
    }
    public void SetPlayerActionPoints()
    {
        playerActionPoints = 2;
    }
    public void UpdateTurnText()
    {
        turnText.text = ("Tura Gracza: " + currentPlayer);
    }
    public void UpdateInfoText(string text)
    {
        infoText.text = text;
    }
    public void UpdateActionPointsText()
    {
        actionPointsText.text = ("Pozostałe punkty akcji: " + playerActionPoints.ToString());
    }
}
