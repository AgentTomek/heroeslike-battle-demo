﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour, IGridUnit
{
    [SerializeField] int maxHealth;
    [SerializeField] int currentHealth;
    [SerializeField] int attack;
    [SerializeField] int attackRange;
    [SerializeField] int movePoints;
    [SerializeField] int actionPoints;
    [SerializeField] UnitType unitType;
    public BattleManager.UnitSide unitSide;
    [SerializeField] UnitData unitData;
    
    private void Start()
    {
        maxHealth = unitData.Health;
        currentHealth = maxHealth;
        attack = unitData.AttackValue;
        attackRange = unitData.AttackRange;
        movePoints = unitData.MovePoints;
        actionPoints = unitData.ActionPoints;
    }
    public int GetUnitMovePoints()
    {
        return movePoints;
    }
    public Vector3 GetUnitPos()
    {
        return this.transform.position;
    }
    public void SetUnitPos(Vector3 posToSet)
    {
        this.transform.position = posToSet;
    }

    public Vector3 GetCurrentPosition()
    {
       return transform.position;
    }

    public int GetUnitAttackValue()
    {
        return attack;
    }

    public int GetUnitAttackRange()
    {
        return attackRange;
    }
    public int GetUnitCurrentHealth()
    {
        return currentHealth;
    }

    public void SetUnitHealthAfterAttack(int attackValue)
    {
        currentHealth = currentHealth - attackValue;
    }

    public void DestroyUnitWhenDied()
    {
            this.gameObject.SetActive(false);
    }

    BattleManager.UnitSide IGridUnit.GetUnitSide()
    {
        return unitSide;
    }
}

