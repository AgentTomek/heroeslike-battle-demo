﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BattleManager;
public interface IGridUnit : IGridElement
{
    UnitSide GetUnitSide();
    int GetUnitMovePoints();
    int GetUnitAttackValue();
    int GetUnitAttackRange();
    int GetUnitCurrentHealth();
    void SetUnitHealthAfterAttack(int attackValue);
    Vector3 GetUnitPos();
    void SetUnitPos(Vector3 posToSet);

    Vector3 GetCurrentPosition();

    void DestroyUnitWhenDied();

}

