﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGridElement
{ 
}
public interface IGridTile : IGridElement
{
    Vector3 GetTilePos();
    bool CheckIfReachable();
}

